var cdb = document.getElementById('change-display');
var fList = document.getElementById('file-list');

cdb.addEventListener('click', changeDisplay);

// Check if the list display is on
function checkDisplay() {
    var list = sessionStorage.getItem('list');
    if (list === 'true') {
        changeDisplay();
    }
}

// Toggle list display
function changeDisplay() {
    fList.classList.toggle('table');
    cdb.classList.toggle('icon-view_list');
    cdb.classList.toggle('icon-view_module');
    if (fList.classList.contains('table')) {
        sessionStorage.setItem('list', true);
    } else {
        sessionStorage.setItem('list', false);
    }
}

checkDisplay();
